package com.company;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class ListenerMainClass {
    public static void main() throws InterruptedException{
        System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        JavascriptExecutor js = (JavascriptExecutor)driver;
        EventFiringWebDriver eventHAndler = new EventFiringWebDriver(driver);
        EventCapture eCapture = new EventCapture();

        eventHAndler.register(eCapture);
        eventHAndler.navigate().to("https://www.edureka.co/blog/");
        js.executeScript("window.scrollBy(0,400)");
        Thread.sleep(3000);
        eventHAndler.findElement(By.linkText("Software Testing")).click();
        eventHAndler.navigate().to("https://www.edureka.co/all-courses");
        eventHAndler.navigate().back();
        eventHAndler.quit();
        eventHAndler.unregister(eCapture);
        System.out.println("End of listeners class");
    }
}
